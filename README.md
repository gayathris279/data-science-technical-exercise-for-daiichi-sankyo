## Getting Started
The jupyter notebook 'main_solution.ipynb' is the submitted solution. 

Folder 'Data' has input train and test data as well as the output predictions data.

Folder 'Models' has the best model stored.

## Overview
The objective of this assignment is to perform EDA on the bank marketing campaign data and to train a prediction model to predict if the product (bank term deposit) would be subscribed ('yes') or not subscribed ('no'). 

Below steps are implemented in the code.

- Performed EDA on the shared training data. 
- Trained and evaluated different machine Learning models on the training data. Selected the best and stored it for further usage.
- Load the stored model and use it to run predictions on the test file.
- Store the 'predictions.csv' file in the Data folder.

Findings and decisions are stored as markdowns in the jupyter notebook('main_solution.ipynb').
